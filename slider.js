$(document).ready(function() {

    var $element = $('input[type="range"]');
    var $handle;

    $element.rangeslider({
        polyfill: false,
        onInit: function() {
            $handle = $('.rangeslider__handle', this.$range);
            updateHandle($handle[0], this.value);
            $("#amount-label").html('<span class="pricing__dollar"></span>' + this.value + '% A favor');
        }
    }).on('input', function() {
        updateHandle($handle[0], this.value);
        $("#amount-label").html('<span class="pricing__dollar"></span>' + this.value + '% A favor');
    });

    function updateHandle(el, val) {
        el.textContent = val;
    }

    $('input[type="range"]').rangeslider();
    
});
